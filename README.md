# vue-radio

Aplikacja Webowa (Vue.js) do słuchania stacji radiowych. Korzysta z DevRadioApi.

Aplikacja działa pod adresem:
https://listen.msdev.ovh/

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
