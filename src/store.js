import Vue from 'vue'
import Vuex from 'vuex'
// import * as http from 'http'
import axios from 'axios'
import config from './config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    stations: []
  },
  mutations: {
    FETCH_STATIONS(state, stations) {
      state.stations = [];
      Object.keys(stations).forEach((gr) => {
        stations[gr].forEach((st) => {
          state.stations.push(st);
        });
      });
    }
  },
  actions: {
    fetchStations({ commit }) {
      axios.get(`${config.API_URL}/radio`)
      .then((response) => {
        commit("FETCH_STATIONS", response.data);
      })
      .catch((error => {
        /* eslint-disable no-alert, no-console */
        console.log(error.statusText);
        /* eslint-enable */
      }))
    }
  },
  getters : {
    STATIONS : state => {
      return state.stations;
    }
  }
})
